import React from 'react';
import './Home.css';
import { logout } from '../../redux/user/actions';
import { useDispatch } from 'react-redux';

function Home() {
    const dispatch = useDispatch();

    const logOut = () => {
        dispatch(logout());
    }

    return (
        <div className="home">
            <div className="home__info">
                <h1>Welcome to Home Page</h1>
                <button onClick={logOut}>Log Out</button>
            </div>
            <div className="home__overlay" />
        </div>
    )
}

export default Home

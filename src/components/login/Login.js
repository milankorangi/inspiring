import React, {useState, useEffect} from 'react'
import './Login.css';
import users from '../../modal/users';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';
import { login } from '../../redux/user/actions';

function Login() {
    const [right, setRight] = useState(false);
    const dispatch = useDispatch();
     
    useEffect(() => {
        let count = false;
        if(right){
            dispatch(login(values.email))
        }
        return () => { count = true };
      }, [right]);
  
    const validationSchema = Yup.object().shape({
        email: Yup.string().required("Required").email("Enter valid email"),
        password: Yup.string()
                    .required('Required')
                    .min(6, "Password is too short - Should be minimum 6 Characters")
                    .matches(/(?=.*[0-9])/, "Password must contain a number.")
    })

    const { handleSubmit, handleChange, handleBlur, touched, isSubmitting, values, errors} = useFormik({
        initialValues: {
            email: "",
            password: ""
        },
        validationSchema,
        onSubmit( values, { setSubmitting }){
            if(users.find(user => user.email === values.email) && users.find(user => user.password === values.password)){
                setRight(true)
            }else if(!users.find(user => user.email === values.email) && !users.find(user => user.password === values.password))
            {
                errors.email = "Invalid email address";
                errors.password = "Invalid password";
            }else if(!users.find(user => user.email === values.email) && users.find(user => user.password === values.password))
            {
                errors.email = "Invalid email address";
            }else if(users.find(user => user.email === values.email) && !users.find(user => user.password === values.password))
            {
                errors.password = "Invalid password";
            }
            setSubmitting(false);
        }
    })
    return (
        <div className="login">
            <div className="login__container">
                <h1>Login</h1>
                <form onSubmit={handleSubmit}>
                    <h4>E-mail</h4>
                    <input type="email" 
                    name="email"
                    placeholder="Enter your email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                    className={errors.email && touched.email && "error"}/>
                    <div className="login__error">
                        {errors.email && touched.email ? errors.email : null}
                    </div>

                    <h4>Password</h4>
                    <input type="password" 
                    name="password"
                    placeholder="Enter your password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                    className={errors.password && touched.password && "error"}/>
                    <div className="login__error">
                        {errors.password && touched.password ? errors.password : null}
                    </div>
                    
                    <button type="submit" className="login__button" >
                        Login
                    </button>
                </form>
            </div>
        </div>
    )
}

export default Login

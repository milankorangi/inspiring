import React from 'react';
import{ Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

const PublicRoute = ({
    component: Component,
    ...rest
}) => {
    const isLogged = useSelector(state => state.reducers.isLogged);

    return <Route {...rest} component={(props)=> (
        !isLogged ? (
            <Component {...props} />
        ) : (
            <Redirect to="/" />
        )
    )} />
}

export default PublicRoute;
import React from 'react';
import{ Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

const PrivateRoute = ({
    component: Component,
    ...rest
}) => {
    const isLogged = useSelector(state => state.reducers.isLogged);
    console.log('from store', isLogged)

    return <Route {...rest} component={(props)=> (
        !isLogged ? (
            <Redirect to="/login" />
        ) : (
            <Component {...props} />
        )
    )} />
}

export default PrivateRoute;
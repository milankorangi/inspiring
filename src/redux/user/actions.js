export function login(user){
    return{
        type: 'SUCCESS_LOGIN',
        user
    }
}

export function logout(){
    return{
        type: 'SUCCESS_LOGOUT',
    }
}

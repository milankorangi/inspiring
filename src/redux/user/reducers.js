const INITIALL_STATE = {
    user: null,
    isLogged: false
}

export default function reducers(state= INITIALL_STATE, action){
    switch(action.type){
        case 'SUCCESS_LOGIN':
            return{
                // ...state,
                isLogged: true,
                user: action.user
            }
        case 'SUCCESS_LOGOUT':
            return{
                // ...state,
                isLogged: false,
                user: null
            }
        default:
            return state;
    }
}

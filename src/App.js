import React from 'react'
import {
  BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";
import Login from './components/login/Login';
import Home from './components/home/Home';
import NoPage from './components/page404/NoPage';
import PrivateRoute from './route/PrivateRoute';
import PublicRoute from './route/PublicRoute';

function App() {
    return (
        <Router>
          <Switch>
            <PrivateRoute path='/' component={Home} exact={true} />
            <PublicRoute path="/login" component={Login} />
            <Route component={NoPage} />
          </Switch>
        </Router>
    )
}

export default App
